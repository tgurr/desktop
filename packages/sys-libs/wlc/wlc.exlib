# Copyright 2015 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

SCM_chck_REPOSITORY="https://github.com/Cloudef/chck"
SCM_waylandprotocols_REPOSITORY="git://anongit.freedesktop.org/wayland/wayland-protocols"
SCM_SECONDARY_REPOSITORIES="chck waylandprotocols"
SCM_EXTERNAL_REFS="lib/chck:chck protos/wayland-protocols:waylandprotocols"

require github [ user=Cloudef release=v${PV} suffix=tar.bz2 ]
require cmake [ api=2 ]

SUMMARY="Wayland compositor library"

LICENCES="MIT"
MYOPTIONS="
    X [[ description = [ X backend and XWayland support ] ]]
    examples
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-apps/dbus
        sys-libs/libinput
        sys-libs/wayland[>=1.7]
        sys-libs/wayland-protocols
        x11-dri/libdrm
        x11-dri/mesa[?X]
        x11-libs/libxkbcommon
        x11-libs/pixman
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        X? (
            x11-libs/libX11
            x11-libs/libxcb
            x11-utils/xcb-util-image
            x11-utils/xcb-util-wm
        )
    run:
        X? ( x11-server/xorg-server[xwayland] )
"

BUGS_TO="markusr815@gmail.com"

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'examples WLC_BUILD_EXAMPLES'
    'X WLC_X11_BACKEND_SUPPORT'
    'X WLC_XWAYLAND_SUPPORT'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DWLC_BUILD_TESTS:BOOL=TRUE -DWLC_BUILD_TESTS:BOOL=FALSE'
)

